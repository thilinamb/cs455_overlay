package cs455.overlay.type;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Thilina
 * Date: 1/31/14
 */
public class OverlayNode {

    public volatile static int counter = 1;
    private List<OverlayLink> links = new ArrayList<OverlayLink>();

    int nodeId;
    private String hostname;
    private int port;

    public OverlayNode(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
        this.nodeId = counter++;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return hostname + ":" + port;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void addLink(OverlayLink link){
        links.add(link);
    }

    public List<OverlayLink> getLinks(){
        return links;
    }

    public int getLinkCount(){
        return links.size();
    }

}
