package cs455.overlay.type;

/**
 * Author: Thilina
 * Date: 2/4/14
 */
public class OverlayLink {

    private OverlayNode src;
    private OverlayNode destination;
    private int weight;

    public OverlayLink(OverlayNode src, OverlayNode destination) {
        this.src = src;
        this.destination = destination;
    }

    public int getWeight() {
        return weight;
    }

    public OverlayNode getSrc() {
        return src;
    }

    public OverlayNode getDestination() {
        return destination;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
