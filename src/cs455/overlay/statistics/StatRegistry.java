package cs455.overlay.statistics;

import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.impl.TrafficSummary;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Thilina
 * Date: 2/15/14
 */
public class StatRegistry {

    private static final StatRegistry instance = new StatRegistry();

    private long totalSentCount;
    private long totalReceiveCount;
    private long totalSumSent;
    private long totalSumReceived;

    private List<TrafficSummary> trafficSummaryList = new ArrayList<TrafficSummary>();

    private StatRegistry() {}

    public static StatRegistry getInstance(){
        return instance;
    }

    public synchronized int processTrafficSummary(TrafficSummary trafficSummary){
        totalSentCount += trafficSummary.getSentCount();
        totalReceiveCount += trafficSummary.getReceivedCount();
        totalSumSent += trafficSummary.getSendSummation();
        totalSumReceived += trafficSummary.getReceivedSummation();
        trafficSummaryList.add(trafficSummary);
        return trafficSummaryList.size();
    }

    public void printTrafficSummary(){
        String format = "%-20s  %-25s %-25s  %-25s   %-25s   %-25s%n";
        String dataFormat = "%-20s  %-25d %-25d %-25d   %-25d   %-25d%n";
        String summaryFormat = "%-20s  %-25d %-25d %-25d   %-25d   %-25s%n";
        System.out.printf(format, "Node","No. of messages Sent",
                "No. of Messages Received", "Sum of Sent messages", "Sum of Received Messages",
                "No. of messages relayed");
        System.out.printf(format, "------------", "--------------------", "-----------------------",
                "--------------------", "----------------------", "----------------------");
        for(TrafficSummary ts : trafficSummaryList){
            System.out.printf(dataFormat, OverlayUtil.getNodeName(ts.getHostAddress(), ts.getPort()),
                    ts.getSentCount(), ts.getReceivedCount(), ts.getSendSummation(), ts.getReceivedSummation(),
                    ts.getRelayedCount());
        }
        System.out.printf(format, "------------", "--------------------", "-----------------------",
                "--------------------", "----------------------", "----------------------");
        System.out.printf(summaryFormat, "Sum", totalSentCount, totalReceiveCount, totalSumSent, totalSumReceived, "");
    }

    public synchronized void reset(){
        totalSentCount = 0;
        totalReceiveCount = 0;
        totalSumReceived = 0;
        totalSumSent = 0;
        trafficSummaryList = new ArrayList<TrafficSummary>();
    }

}
