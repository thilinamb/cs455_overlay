package cs455.overlay.statistics;

/**
 * Author: Thilina
 * Date: 2/8/14
 */
public class StatCollector {

    private static StatCollector instance = new StatCollector();

    private int sendTracker = 0;
    private int receiveTracker = 0;
    private int relayTracker = 0;
    private long sendSummation = 0;
    private long receiveSummation = 0;

    private StatCollector(){}

    public static StatCollector getInstance(){
        return instance;
    }

    public synchronized void incrementSentCount(){
        sendTracker++;
    }

    public synchronized void incrementReceiveCount(){
        receiveTracker++;
    }

    public synchronized void incrementRelayCount(){
        relayTracker++;
    }

    public synchronized void incrementSendSummation(long val){
        sendSummation += val;
    }

    public synchronized void incrementReceiveSummation(long val){
        receiveSummation += val;
    }

    public synchronized void reset(){
        sendTracker = 0;
        receiveTracker = 0;
        receiveTracker = 0;
        sendSummation = 0;
        receiveSummation = 0;
    }

    public int getSendTracker() {
        return sendTracker;
    }

    public int getReceiveTracker() {
        return receiveTracker;
    }

    public int getRelayTracker(){
        return relayTracker;
    }

    public long getSendSummation() {
        return sendSummation;
    }

    public long getReceiveSummation() {
        return receiveSummation;
    }
}
