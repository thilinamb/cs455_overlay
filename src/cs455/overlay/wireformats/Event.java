package cs455.overlay.wireformats;

import java.io.IOException;

/**
 * User: thilinab
 * Date: 1/29/14
 * Time: 1:08 PM
 */
public interface Event {

    public int getType();

    public byte[] getBytes() throws IOException;

    public Event createEvent(byte[] data) throws IOException;

}
