package cs455.overlay.wireformats;

/**
 * User: thilinab
 * Date: 1/29/14
 * Time: 1:22 PM
 */
public class Protocol {
    // registration
    public static final int REGISTER_REQ = 200;
    public static final int REGISTER_RESP = 211;
    public static final int DEREGISTER_REQ = 220;
    public static final int DEREGISTER_RESP = 221;

    // setting up the overlay
    public static final int MESSAGING_NODE_LIST = 300;
    public static final int LINK_WEIGHTS = 310;

    // sending messages
    public static final int TASK_INITIATE = 500;
    public static final int MESSAGE = 510;
    public static final int TASK_COMPLETE = 520;

    // statistics
    public static final int PULL_TRAFFIC_SUMMARY = 600;
    public static final int TRAFFIC_SUMMARY = 610;

}
