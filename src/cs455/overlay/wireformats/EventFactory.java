package cs455.overlay.wireformats;

import cs455.overlay.exception.NoSuchEventException;
import cs455.overlay.util.OverlayUtil;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Thilina
 * Date: 1/29/14
 * Event Factory that creates events out of byte streams.
 * Implements the factory pattern.
 */
public class EventFactory {
    /**
     * Singleton instance
     */
    private static EventFactory instance = new EventFactory();

    private Map<Integer, Event> eventObjMap = new HashMap<Integer, Event>();

    private EventFactory(){
        // Singleton pattern: private constructor
    }

    /**
     * Returns the singleton instance
     * @return the singleton instance of EventFactory
     */
    public static EventFactory getInstance(){
        return instance;
    }

    public void registerEventType(int eventType, Event eventObj){
        eventObjMap.put(eventType, eventObj);
    }

    public Event getEvent(byte[] data) throws NoSuchEventException, IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStream = new DataInputStream(byteArrInStream);
        try {
            int eventType = dataInStream.readInt();
            Event event = eventObjMap.get(eventType);
            if(event != null){
                return event.createEvent(data);
            } else {
                OverlayUtil.logError(this.getClass(), "No such event type." + eventType);
                throw new NoSuchEventException(eventType);
            }
        } finally {
            byteArrInStream.close();
            dataInStream.close();
        }
    }
}
