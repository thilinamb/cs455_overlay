package cs455.overlay.wireformats.impl;

import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;

/**
 * Author: Thilina
 * Date: 2/10/14
 */
public class TaskComplete implements Event {

    static {
        EventFactory.getInstance().registerEventType(Protocol.TASK_COMPLETE, new TaskComplete());
    }

    private int type = Protocol.TASK_COMPLETE;
    private String hostAddress;
    private int port;

    public TaskComplete() {
    }

    public TaskComplete(String hostAddress, int port) {
        this.hostAddress = hostAddress;
        this.port = port;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write data in the order
        dataOutStr.writeInt(type);
        // write hostname
        dataOutStr.writeUTF(hostAddress);
        // write port
        dataOutStr.writeInt(port);
        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();
        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);
        // first read the type and discard it.
        dataInStr.readInt();
        // read the hostname and port
        String hostAddress = dataInStr.readUTF();
        int port = dataInStr.readInt();

        byteArrInStream.close();
        dataInStr.close();

        return new TaskComplete(hostAddress, port);
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public int getPort() {
        return port;
    }
}
