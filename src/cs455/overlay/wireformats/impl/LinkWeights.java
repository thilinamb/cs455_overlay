package cs455.overlay.wireformats.impl;

import cs455.overlay.type.OverlayLink;
import cs455.overlay.type.OverlayNode;
import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Thilina
 * Date: 2/5/14
 */
public class LinkWeights implements Event {

    private int type = Protocol.LINK_WEIGHTS;
    private int linkCount;
    private List<OverlayLink> links = new ArrayList<OverlayLink>();

    static {
        EventFactory.getInstance().registerEventType(Protocol.LINK_WEIGHTS, new LinkWeights());
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write type
        dataOutStr.writeInt(type);
        // write link count
        dataOutStr.writeInt(linkCount);
        // start writing the links.
        StringBuilder sBuilder = new StringBuilder();
        for(OverlayLink link : links){
            sBuilder.append(link.getSrc());
            sBuilder.append('#');
            sBuilder.append(link.getDestination());
            sBuilder.append('#');
            sBuilder.append(link.getWeight());
            sBuilder.append(',');
        }
        sBuilder.deleteCharAt(sBuilder.length() -1);
        dataOutStr.writeUTF(sBuilder.toString());

        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();

        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);

        // read the type
        dataInStr.readInt();
        // read the no of links
        int linkCount = dataInStr.readInt();
        // read the set of links encoded as a String.
        List<OverlayLink> links = new ArrayList<OverlayLink>(linkCount);
        String linksStr = dataInStr.readUTF();
        String[] linkStrArray = linksStr.split(",");
        for(String linkStr : linkStrArray){
            String[] nodeAndWeight = linkStr.split("#");
            String[] hostAndPort1 = nodeAndWeight[0].split(":");
            OverlayNode node1 = new OverlayNode(hostAndPort1[0], Integer.parseInt(hostAndPort1[1]));
            String[] hostAndPort2 = nodeAndWeight[1].split(":");
            OverlayNode node2 = new OverlayNode(hostAndPort2[0], Integer.parseInt(hostAndPort2[1]));
            int weight = Integer.parseInt(nodeAndWeight[2]);
            OverlayLink overlayLink = new OverlayLink(node1, node2);
            overlayLink.setWeight(weight);
            links.add(overlayLink);
        }

        // construct the event
        LinkWeights linkWeights = new LinkWeights();
        linkWeights.setLinkCount(linkCount);
        linkWeights.setLinks(links);

        dataInStr.close();
        byteArrInStream.close();

        return linkWeights;
    }

    public int getLinkCount() {
        return linkCount;
    }

    public void setLinkCount(int linkCount) {
        this.linkCount = linkCount;
    }

    public List<OverlayLink> getLinks() {
        return links;
    }

    public void setLinks(List<OverlayLink> links) {
        this.links = links;
    }
}
