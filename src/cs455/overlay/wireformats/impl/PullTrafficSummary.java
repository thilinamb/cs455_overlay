package cs455.overlay.wireformats.impl;

import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Author: Thilina
 * Date: 2/15/14
 */
public class PullTrafficSummary implements Event {

    static {
        EventFactory.getInstance().registerEventType(Protocol.PULL_TRAFFIC_SUMMARY, new PullTrafficSummary());
    }

    private int type = Protocol.PULL_TRAFFIC_SUMMARY;

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write data in the order
        dataOutStr.writeInt(type);

        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();
        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        return new PullTrafficSummary();
    }
}
