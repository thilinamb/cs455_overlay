package cs455.overlay.wireformats.impl;

import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;

/**
 * Author: Thilina
 * Date: 2/8/14
 */
public class Message implements Event {

    private int type = Protocol.MESSAGE;

    static {
        EventFactory.getInstance().registerEventType(Protocol.MESSAGE, new Message());
    }

    private long value;
    private String destination;

    public Message(){

    }

    public Message(long value, String destination) {
        this.value = value;
        this.destination = destination;
    }

    public long getValue() {
        return value;
    }

    public String getDestination() {
        return destination;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write data in the order
        dataOutStr.writeInt(type);
        // write the value
        dataOutStr.writeLong(value);
        // write the dest
        dataOutStr.writeUTF(destination);

        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();
        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);
        // first read the type and discard it.
        dataInStr.readInt();
        // read the long value
        long value = dataInStr.readLong();
        // read the destination
        String dest = dataInStr.readUTF();
        // construct the object
        Message message = new Message(value, dest);
        byteArrInStream.close();
        dataInStr.close();

        return message;
    }
}
