package cs455.overlay.wireformats.impl;

import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;

/**
 * Author: Thilina
 * Date: 1/29/14
 * OverlayNode Registration Request sent to the Registry.
 * Format is as follows.

     Message Type (int): REGISTER_REQUEST
     IP address (String)
     Port number (int)

 */
public class RegistrationRequest implements Event {

    private String sourceIpAddress;
    private int port;

    static {
        EventFactory.getInstance().registerEventType(Protocol.REGISTER_REQ, new RegistrationRequest());
    }

    @Override
    public int getType() {
        return Protocol.REGISTER_REQ;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write data in the order
        dataOutStr.writeInt(Protocol.REGISTER_REQ);
        dataOutStr.writeUTF(sourceIpAddress);
        dataOutStr.writeInt(port);

        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();
        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);
        // first read the type and discard it.
        dataInStr.readInt();
        // ip address
        String ipAddress = dataInStr.readUTF();
        // port
        int port = dataInStr.readInt();
        RegistrationRequest registrationReq = new RegistrationRequest();
        registrationReq.setSourceIpAddress(ipAddress);
        registrationReq.setPort(port);

        byteArrInStream.close();
        dataInStr.close();

        return registrationReq;
    }

    public String getSourceIpAddress() {
        return sourceIpAddress;
    }

    public void setSourceIpAddress(String sourceIpAddress) {
        this.sourceIpAddress = sourceIpAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
