package cs455.overlay.wireformats.impl;

import cs455.overlay.type.OverlayNode;
import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Thilina
 * Date: 2/4/14
 */
public class MessagingNodeList implements Event {

    private static int type = Protocol.MESSAGING_NODE_LIST;

    static {
        EventFactory.getInstance().registerEventType(Protocol.MESSAGING_NODE_LIST, new MessagingNodeList());
    }

    private int nodeCount;
    private List<OverlayNode> nodes = new ArrayList<OverlayNode>();

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write data
        // type
        dataOutStr.writeInt(type);
        // node count
        dataOutStr.writeInt(nodeCount);

        // prepare a string representing all the nodes
        StringBuilder nodesStr = new StringBuilder();
        for(OverlayNode node : nodes){
            nodesStr.append(node);
            nodesStr.append(',');
        }
        nodesStr.deleteCharAt(nodesStr.length()-1);
        dataOutStr.writeUTF(nodesStr.toString());

        // prepare for shipment
        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();

        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);
        // read the type
        dataInStr.readInt();
        // read the number of nodes
        int nodeCount = dataInStr.readInt();
        // now read the node list as a String. Each node info is separated by a ','
        String nodeListStr = dataInStr.readUTF();
        String[] nodes = nodeListStr.split(",");
        // let's assume that the number of nodes and the node count are matched always.
        List<OverlayNode> nodeList = new ArrayList<OverlayNode>();
        for(String nodeStr : nodes){
            String[] info = nodeStr.split(":");
            OverlayNode node = new OverlayNode(info[0], Integer.parseInt(info[1]));
            nodeList.add(node);
        }
        MessagingNodeList messagingNodeList = new MessagingNodeList();
        messagingNodeList.setNodeCount(nodeCount);
        messagingNodeList.setNodes(nodeList);

        byteArrInStream.close();
        dataInStr.close();

        return messagingNodeList;
    }

    public int getNodeCount() {
        return nodeCount;
    }

    public void setNodeCount(int nodeCount) {
        this.nodeCount = nodeCount;
    }

    public List<OverlayNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<OverlayNode> nodes) {
        this.nodes = nodes;
    }
}
