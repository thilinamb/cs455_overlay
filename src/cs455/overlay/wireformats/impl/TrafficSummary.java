package cs455.overlay.wireformats.impl;

import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;

/**
 * Author: Thilina
 * Date: 2/15/14
 */
public class TrafficSummary implements Event {

    static {
        EventFactory.getInstance().registerEventType(Protocol.TRAFFIC_SUMMARY, new TrafficSummary());
    }

    private int type = Protocol.TRAFFIC_SUMMARY;
    private String hostAddress;
    private int port;
    private long sentCount;
    private long receivedCount;
    private long relayedCount;
    private long sendSummation;
    private long receivedSummation;

    public TrafficSummary() {
    }

    public TrafficSummary(String hostAddress, int port,
                          long sentCount, long receivedCount,
                          long relayedCount, long sendSummation,
                          long receivedSummation) {
        this.hostAddress = hostAddress;
        this.port = port;
        this.sentCount = sentCount;
        this.receivedCount = receivedCount;
        this.relayedCount = relayedCount;
        this.sendSummation = sendSummation;
        this.receivedSummation = receivedSummation;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write data in the order
        dataOutStr.writeInt(type);
        // write hostname
        dataOutStr.writeUTF(hostAddress);
        // write port
        dataOutStr.writeInt(port);
        // write sent count
        dataOutStr.writeLong(sentCount);
        // write received count
        dataOutStr.writeLong(receivedCount);
        // write relayed count
        dataOutStr.writeLong(relayedCount);
        // write sent sum
        dataOutStr.writeLong(sendSummation);
        // write received sum
        dataOutStr.writeLong(receivedSummation);

        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();
        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);
        // first read the type and discard it.
        dataInStr.readInt();
        // read the hostname
        String hostname = dataInStr.readUTF();
        // read the port
        int port = dataInStr.readInt();
        // read send count
        long sentCount = dataInStr.readLong();
        // read the received count
        long receivedCount = dataInStr.readLong();
        // read relayed count
        long relayedCount = dataInStr.readLong();
        // read sent sum
        long sentSum = dataInStr.readLong();
        // read received sum
        long receivedSum = dataInStr.readLong();

        byteArrInStream.close();
        dataInStr.close();

        return new TrafficSummary(hostname, port, sentCount, receivedCount, relayedCount, sentSum,
                receivedSum);
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public int getPort() {
        return port;
    }

    public long getSentCount() {
        return sentCount;
    }

    public long getReceivedCount() {
        return receivedCount;
    }

    public long getRelayedCount() {
        return relayedCount;
    }

    public long getSendSummation() {
        return sendSummation;
    }

    public long getReceivedSummation() {
        return receivedSummation;
    }
}
