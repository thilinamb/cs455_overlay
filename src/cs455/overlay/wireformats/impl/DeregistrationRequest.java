package cs455.overlay.wireformats.impl;

import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;

/**
 * Author: Thilina
 * Date: 1/31/14
 */
public class DeregistrationRequest implements Event {

    private int type = Protocol.DEREGISTER_REQ;
    private String sourceIp;
    private int port;

    static {
        EventFactory.getInstance().registerEventType(Protocol.DEREGISTER_REQ, new DeregistrationRequest());
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));

        // write data in the order
        dataOutStr.writeInt(type);
        dataOutStr.writeUTF(sourceIp);
        dataOutStr.writeInt(port);

        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();
        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);
        // first read the type and discard it.
        dataInStr.readInt();
        // read source ip
        String source = dataInStr.readUTF();
        // read port
        int port = dataInStr.readInt();
        // initialize the object
        DeregistrationRequest deRegReq = new DeregistrationRequest();
        deRegReq.setSourceIp(source);
        deRegReq.setPort(port);

        byteArrInStream.close();
        dataInStr.close();

        return deRegReq;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
