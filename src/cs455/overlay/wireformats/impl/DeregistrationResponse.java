package cs455.overlay.wireformats.impl;

import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;
import cs455.overlay.wireformats.Protocol;

import java.io.*;

/**
 * Author: Thilina
 * Date: 1/31/14
 */
public class DeregistrationResponse implements Event {

    private int type = Protocol.DEREGISTER_RESP;
    private boolean success;
    private String message;

    static{
        EventFactory.getInstance().registerEventType(Protocol.DEREGISTER_RESP, new DeregistrationResponse());
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public byte[] getBytes() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutStr = new DataOutputStream(new BufferedOutputStream(baOutputStream));
        // write data
        dataOutStr.writeInt(type);
        dataOutStr.writeInt(success ? 1 : 0);
        dataOutStr.writeUTF(message);
        // prepare for shipment
        dataOutStr.flush();
        byte[] marshalledBytes = baOutputStream.toByteArray();

        baOutputStream.close();
        dataOutStr.close();

        return marshalledBytes;
    }

    @Override
    public Event createEvent(byte[] data) throws IOException {
        ByteArrayInputStream byteArrInStream = new ByteArrayInputStream(data);
        DataInputStream dataInStr = new DataInputStream(byteArrInStream);
        // read the type
        dataInStr.readInt();
        // read the success
        int status = dataInStr.readInt();
        boolean success = (status == 1)? true : false;
        String message = dataInStr.readUTF();

        DeregistrationResponse regResp = new DeregistrationResponse();
        regResp.setSuccess(success);
        regResp.setMessage(message);

        byteArrInStream.close();
        dataInStr.close();

        return regResp;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
