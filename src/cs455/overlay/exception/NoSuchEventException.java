package cs455.overlay.exception;

/**
 * Author: Thilina
 * Date: 1/29/14
 */
public class NoSuchEventException extends OverlayException {

    public NoSuchEventException(int eventType) {
        super("Event Type " + eventType + " is not found.");
    }

}
