package cs455.overlay.exception;

/**
 * Author: Thilina
 * Date: 1/29/14
 */
public class OverlayException extends Exception {

    public OverlayException() {
        super();
    }

    public OverlayException(String message) {
        super(message);
    }

    public OverlayException(String message, Throwable cause) {
        super(message, cause);
    }
}
