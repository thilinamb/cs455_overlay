package cs455.overlay.util;

import cs455.overlay.node.Node;
import cs455.overlay.transport.Connection;
import cs455.overlay.transport.ConnectionFactory;
import cs455.overlay.wireformats.Event;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Author: Thilina
 * Date: 1/31/14
 * A utility class for messaging related functionality.
 */
public class MessagingUtil {

    public static void sendMessage(String destAddress, int destPort, Event event, InetAddress srcAddr, Node node)
            throws IOException{
        Connection conn = ConnectionFactory.getInstance().getConnection(destAddress, destPort, srcAddr, node);
        conn.sendData(event.getBytes());
    }
}
