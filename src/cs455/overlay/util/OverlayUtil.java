package cs455.overlay.util;

import cs455.overlay.wireformats.impl.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;

/**
 * Author: Thilina
 * Date: 1/29/14
 * A class with utility methods.
 */
public class OverlayUtil {

    private static Random rand = null;

    public static void initEventFactory(){
        new RegistrationRequest();
        new RegistrationResponse();
        new DeregistrationRequest();
        new DeregistrationResponse();
        new MessagingNodeList();
        new LinkWeights();
        new TaskInitiate();
        new Message();
        new TaskComplete();
        new PullTrafficSummary();
        new TrafficSummary();
    }

    public static void initRandomNumberGenerator(int port){
        rand = new Random(port);
    }

    public static InetAddress getHostInetAddress() throws UnknownHostException {
            InetAddress inetAddr = InetAddress.getLocalHost();
            return inetAddr;
    }

    public static String getNodeName(String hostname, int port){
        return hostname + ":" + port;
    }

    public static long getNextLong(){
        long x = 2147483647L;
        long y = -2147483648L;
        long number = x+((long)(rand.nextDouble()*(y-x)));
        return number;
    }

    public static void logInfo(Class logClass, String message){
        if (message != null) {
            System.out.println("[INFO]" + logClass.getName() + ": " + message);
        }
    }

    public static void logError(Class logClass, String message){
        if (message != null) {
            System.err.println("[ERROR]" + logClass.getName() + ": " + message);
        }
    }

}