package cs455.overlay.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Author: Thilina
 * Date: 1/31/14
 */
public class CommandProcessor {
    public static final int EXIT_OVERLAY = 5001;
    public static final int LIST_MESSAGING_NODES = 5011;
    public static final int SETUP_OVERLAY = 5012;
    public static final int SEND_OVERLAY_LINK_WEIGHTS = 5013;
    public static final int LIST_WEIGHTS = 5014;
    public static final int PRINT_SHORTEST_PATH = 5014;
    public static final int START = 5020;

    private Map<String, Integer> commandList = new HashMap<String, Integer>();
    private static CommandProcessor instance = new CommandProcessor();

    private CommandProcessor() {
        init();
    }

    public static CommandProcessor getInstance() {
        return instance;
    }

    private void init() {
        commandList.put("exit-overlay", EXIT_OVERLAY);
        commandList.put("list-messaging-nodes", LIST_MESSAGING_NODES);
        commandList.put("setup-overlay", SETUP_OVERLAY);
        commandList.put("send-overlay-link-weights", SEND_OVERLAY_LINK_WEIGHTS);
        commandList.put("list-weights", LIST_WEIGHTS);
        commandList.put("print-shortest-path", PRINT_SHORTEST_PATH);
        commandList.put("start", START);
    }

    public int getCommandIndex(String command) {
        String commandLowerCase = command.toLowerCase();

        if (commandList.containsKey(commandLowerCase)) {
            return commandList.get(commandLowerCase);
        } else {
            return -1;
        }
    }

}
