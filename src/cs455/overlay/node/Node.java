package cs455.overlay.node;

import cs455.overlay.wireformats.Event;

/**
 * User: thilinab
 * Date: 1/29/14
 * Time: 1:08 PM
 */
public interface Node {

    public void onEvent(Event event, String sourceIp);

}
