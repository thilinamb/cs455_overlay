package cs455.overlay.node;

import cs455.overlay.mnode.CommandHandler;
import cs455.overlay.mnode.EventHandler;
import cs455.overlay.transport.ServerConnectionHandler;
import cs455.overlay.util.CommandProcessor;
import cs455.overlay.util.MessagingUtil;
import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.Protocol;
import cs455.overlay.wireformats.impl.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

/**
 * Author: Thilina
 * Date: 1/31/14
 */
public class MessagingNode implements Node {

    public static final int STATE_DEFAULT = 0;
    public static final int STATE_NODES_POPULATED = 1;
    public static final int STATE_READY_FOR_ROUTING = 2;

    private String registryHost;
    private int registryPort;
    private int localPort;
    private InetAddress localAddress;
    private ServerConnectionHandler connectionHandler;
    private CommandHandler commandHandler;
    private EventHandler eventHandler;
    private volatile boolean isRegistered;
    // keep track of the current state of the registry.
    private int state = STATE_DEFAULT;

    public MessagingNode(String registryHost, int registryPort) throws IOException {
        this.registryHost = registryHost;
        this.registryPort = registryPort;
        localAddress = OverlayUtil.getHostInetAddress();
    }

    public void initialize() throws IOException {
        connectionHandler = new ServerConnectionHandler(this);
        commandHandler = new CommandHandler(this);
        eventHandler = new EventHandler(this);
        this.localPort = connectionHandler.getLocalPort();
        OverlayUtil.initEventFactory();
        OverlayUtil.initRandomNumberGenerator(this.localPort);
        connectionHandler.start();
    }

    public void register() throws IOException {
        RegistrationRequest regReq = new RegistrationRequest();
        regReq.setSourceIpAddress(localAddress.getHostAddress());
        regReq.setPort(localPort);
        MessagingUtil.sendMessage(registryHost, registryPort, regReq, getLocalAddress(), this);
    }

    @Override
    public void onEvent(Event event, String sourceIp) {
        int eventType = event.getType();
        switch (eventType) {
            case Protocol.REGISTER_RESP:
                eventHandler.handleRegistrationResp((RegistrationResponse) event);
                break;
            case Protocol.DEREGISTER_RESP:
                eventHandler.handleDeregistrationResp((DeregistrationResponse) event);
                break;
            case Protocol.MESSAGING_NODE_LIST:
                eventHandler.handleMessagingNodeList((MessagingNodeList) event);
                break;
            case Protocol.LINK_WEIGHTS:
                eventHandler.handleLinkWeights((LinkWeights) event);
                break;
            case Protocol.TASK_INITIATE:
                eventHandler.handleTaskInitiate();
                break;
            case Protocol.MESSAGE:
                eventHandler.handleMessage((Message) event);
                break;
            case Protocol.PULL_TRAFFIC_SUMMARY:
                eventHandler.handlePullTrafficSummary();
                break;
            default:
                OverlayUtil.logError(this.getClass(), "Unsupported event type." + event.getType());
                break;
        }
    }

    public static void main(String[] args) {
        // check whether the registry host and port are passed as arguments
        if (args.length < 2) {
            OverlayUtil.logError(MessagingNode.class,
                    "Missing arguments. Registry host and Port number should be specified!");
            return;
        }
        // read the registry host
        String registryHost = args[0];
        // parse the registry port
        int registryPort = Integer.parseInt(args[1]);

        OverlayUtil.logInfo(MessagingNode.class, "Using Registry Host: " + registryHost
                + " and Registry Port: " + registryPort + ".");

        // create the messaging node
        MessagingNode mNode;
        try {
            mNode = new MessagingNode(registryHost, registryPort);
        } catch (IOException e) {
            OverlayUtil.logError(MessagingNode.class, "Error starting the message node!, " + e.getMessage());
            return;
        }
        // initialize the node
        try {
            mNode.initialize();
        } catch (IOException e) {
            OverlayUtil.logError(MessagingNode.class, "Error in node startup!, " + e.getMessage());
            return;
        }
        OverlayUtil.logInfo(MessagingNode.class, "Messaging Node Started on " +
                mNode.getLocalAddress().getHostAddress() + ":" + mNode.localPort);

        // register
        try {
            mNode.register();
        } catch (IOException e) {
            OverlayUtil.logError(MessagingNode.class, "Error in node Registration!, " + e.getMessage());
            return;
        }

        // wait till registration is completed
        while(!mNode.isRegistered){

        }

        // Now it's ready to accept commands.
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                String command = consoleReader.readLine();
                if(command.trim().equals("") || command.trim().equals(" ") || command.trim().equals("\t")
                        || command.trim().equals("\n")) {
                    continue;
                }
                mNode.handleCommand(command);
            } catch (IOException e) {
                OverlayUtil.logError(MessagingNode.class, "Console Reading Error!, " + e.getMessage());
                continue;
            }
        }
    }

    public void handleCommand(String command) {
        int commandIndex = CommandProcessor.getInstance().getCommandIndex(command);
        switch (commandIndex) {
            case CommandProcessor.EXIT_OVERLAY:
                commandHandler.handleDeregistration();
                break;
            case CommandProcessor.PRINT_SHORTEST_PATH:
                commandHandler.handlePrintShortestPath();
                break;
            default:
                OverlayUtil.logError(this.getClass(), "Unsupported Command " + command);
                break;
        }
    }

    public int getLocalPort() {
        return localPort;
    }

    public InetAddress getLocalAddress() {
        return localAddress;
    }

    public String getRegistryHost() {
        return registryHost;
    }

    public int getRegistryPort() {
        return registryPort;
    }

    public void setRegistered(boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    // Make sure that actions are taken only when the messaging node is in the correct state.
    public synchronized int setState(int newState){
        if(newState == STATE_NODES_POPULATED){
            if(state == STATE_DEFAULT){
                state = STATE_NODES_POPULATED;
            }
        } else if(newState == STATE_READY_FOR_ROUTING){
            if(state == STATE_NODES_POPULATED){
                state = STATE_READY_FOR_ROUTING;
            }
        }
        return state;
    }

    public synchronized int getState(){
        return state;
    }
}
