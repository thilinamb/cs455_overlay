package cs455.overlay.node;

import cs455.overlay.registry.CommandHandler;
import cs455.overlay.registry.EventHandler;
import cs455.overlay.transport.ServerConnectionHandler;
import cs455.overlay.util.CommandProcessor;
import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.Protocol;
import cs455.overlay.wireformats.impl.DeregistrationRequest;
import cs455.overlay.wireformats.impl.RegistrationRequest;
import cs455.overlay.wireformats.impl.TaskComplete;
import cs455.overlay.wireformats.impl.TrafficSummary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

/**
 * User: thilinab
 * Date: 1/29/14
 * Time: 1:53 PM
 * The Registry implementation which extends Node.
 * The registry node is started from the main method of this
 * class. Port number should be passed as a program argument.
 */
public class Registry implements Node {

    private ServerConnectionHandler connHandler;
    private CommandHandler commandHandler;
    private EventHandler eventHandler;
    private InetAddress localAddress;
    private int port;

    public Registry(int port) throws IOException {
        this.port = port;
        localAddress = OverlayUtil.getHostInetAddress();
        OverlayUtil.initEventFactory();
    }

    private void start() throws IOException {
        connHandler = new ServerConnectionHandler(this, port);
        commandHandler = new CommandHandler(this);
        eventHandler = new EventHandler(this);
        connHandler.start();
    }

    @Override
    public void onEvent(Event event, String sourceIp) {
        int type = event.getType();
            switch (type) {
                case Protocol.REGISTER_REQ:
                    eventHandler.handleRegistrationReq((RegistrationRequest) event, sourceIp);
                    break;
                case Protocol.DEREGISTER_REQ:
                    eventHandler.handleDeregistration((DeregistrationRequest) event, sourceIp);
                    break;
                case Protocol.TASK_COMPLETE:
                    eventHandler.handleTaskComplete((TaskComplete)event);
                    break;
                case Protocol.TRAFFIC_SUMMARY:
                    eventHandler.handleTrafficSummary((TrafficSummary)event);
                    break;
                default:
                    OverlayUtil.logError(this.getClass(), "Unsupported Event Type " + event.getType());
                    break;
            }
    }

    public static void main(String[] args) {
        // check whether the port is passed as an argument
        if (args.length == 0) {
            System.out.println("Port number is not specified!");
            return;
        }
        // parse the port number
        int port = Integer.parseInt(args[0]);
        // validate
        if (port <= 1024 || port > 65536) {
            System.out.println("Invalid Port number. Use a number between 1024 and 65536. !");
            return;
        }

        Registry registry = null;
        try {
            registry = new Registry(port);
            registry.start();
            OverlayUtil.logInfo(registry.getClass(), "Registry is started up and listening on port " + port);
        } catch (IOException e) {
            OverlayUtil.logError(registry.getClass(), "Error starting the registry." + e.getMessage());
        }

        // listen to user commands
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        while(true){
            try {
                String command = consoleReader.readLine();
                if(command.trim().equals("") || command.trim().equals(" ") || command.trim().equals("\t")
                        || command.trim().equals("\n")) {
                    continue;
                }
                registry.handleCommand(command);
            } catch (IOException e) {
                OverlayUtil.logError(registry.getClass(), "Console Reading Error! " + e.getMessage());
                continue;
            }
        }


    }

    public void handleCommand(String command) {
        // handle overlay separately because it passes a parameter
            if (command.toLowerCase().startsWith("setup-overlay")) {
                String[] tokenizedCommand = command.split(" ");
                int linkCount = 4;
                if (tokenizedCommand.length == 2) {
                    linkCount = Integer.parseInt(tokenizedCommand[1]);
                }
                commandHandler.handleSetupOverlay(linkCount);
                return;
            }

            int commandIndex = CommandProcessor.getInstance().getCommandIndex(command);
            switch (commandIndex){
                case CommandProcessor.LIST_MESSAGING_NODES:
                    commandHandler.handleListingNodes();
                    break;
                case CommandProcessor.SEND_OVERLAY_LINK_WEIGHTS:
                    commandHandler.handleSendOverlayLinkWeights();
                    break;
                case CommandProcessor.START:
                    commandHandler.handleStart();
                    break;
                case CommandProcessor.LIST_WEIGHTS:
                    commandHandler.handleListWeights();
                    break;
                default:
                    OverlayUtil.logError(this.getClass(), "Unsupported Command : " + command);
                    break;
            }

    }

    public InetAddress getLocalAddress() {
        return localAddress;
    }
}
