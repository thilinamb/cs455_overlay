package cs455.overlay.mnode;

import cs455.overlay.dijkstra.Link;
import cs455.overlay.dijkstra.Vertex;
import cs455.overlay.node.MessagingNode;
import cs455.overlay.statistics.StatCollector;
import cs455.overlay.type.OverlayLink;
import cs455.overlay.type.OverlayNode;
import cs455.overlay.util.MessagingUtil;
import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.impl.*;

import java.io.IOException;
import java.util.*;

/**
 * Author: Thilina
 * Date: 2/4/14
 */
public class EventHandler {

    private final MessagingNode mNode;

    public EventHandler(MessagingNode mNode) {
        this.mNode = mNode;
    }

    public void handleRegistrationResp(RegistrationResponse regResp) {
        // check the status
        if (!regResp.isSuccess()) {
            OverlayUtil.logError(this.getClass(), "Registration Failed!" + regResp.getMessage());
        } else {
            OverlayUtil.logInfo(this.getClass(), regResp.getMessage());
            mNode.setRegistered(true);
        }
    }

    public void handleDeregistrationResp(DeregistrationResponse deregResp) {
        // check the status
        if (!deregResp.isSuccess()) {
            OverlayUtil.logError(this.getClass(), "Deregistration Failed!" + deregResp.getMessage());
        } else {
            OverlayUtil.logInfo(this.getClass(), deregResp.getMessage());
            mNode.setRegistered(false);
        }
    }

    public void handleMessagingNodeList(MessagingNodeList msgNodeList) {
        RoutingRegistry routingRegistry = RoutingRegistry.getInstance();
        List<OverlayNode> nodes = msgNodeList.getNodes();
        for (OverlayNode node : nodes) {
            routingRegistry.addNeighbour(node);
        }
        mNode.setState(MessagingNode.STATE_NODES_POPULATED);
    }

    public void handleLinkWeights(LinkWeights linkWeights) {
        while (mNode.getState() != MessagingNode.STATE_NODES_POPULATED){
            // wait till messaging node list is populated.
        }
        RoutingRegistry routingRegistry = RoutingRegistry.getInstance();
        List<OverlayLink> links = linkWeights.getLinks();
        for (OverlayLink link : links) {
            routingRegistry.addLink(link);
        }
        populateVertices(routingRegistry.getLinks());
        mNode.setState(MessagingNode.STATE_READY_FOR_ROUTING);
    }

    public void handleTaskInitiate() {
        while(mNode.getState() != MessagingNode.STATE_READY_FOR_ROUTING){
            // wait till it's ready for routing.
        }
        RoutingRegistry routingRegistry = RoutingRegistry.getInstance();
        Vertex[] vertices = routingRegistry.getVertices();
        String currentNode = mNode.getLocalAddress().getHostAddress() + ":" + mNode.getLocalPort();
        for (int i = 0; i < 5000; i++) {
            Random random = new Random(System.currentTimeMillis() + mNode.getLocalPort());
            Vertex vertex = vertices[random.nextInt(vertices.length)];
            // check if the selected node is as same as the current node.
            while (vertex.getId().equals(currentNode)) {
                vertex = vertices[random.nextInt(vertices.length)];
            }
            RoutingEntry routingEntry = routingRegistry.getRoutingEntry(vertex.getId());
            String dest = routingEntry.getDest();
            String nextHopStr = routingEntry.getNextHop();
            String[] nextHopData = nextHopStr.split(":");
            String hostName = nextHopData[0];
            int port = Integer.parseInt(nextHopData[1]);
            try {
                for (int j = 0; j < 5; j++) {
                    long value = OverlayUtil.getNextLong();
                    Message msg = new Message(value, dest);
                    MessagingUtil.sendMessage(hostName, port, msg, mNode.getLocalAddress(), mNode);
                    StatCollector.getInstance().incrementSentCount();
                    StatCollector.getInstance().incrementSendSummation(value);
                }
            } catch (IOException e) {
                OverlayUtil.logError(this.getClass(), "Error sending message to " + nextHopStr + ", " + e.getMessage());
            }
        }
        // send back the task complete message
        TaskComplete taskComplete = new TaskComplete(mNode.getLocalAddress().getHostAddress(), mNode.getLocalPort());
        try {
            MessagingUtil.sendMessage(mNode.getRegistryHost(), mNode.getRegistryPort(),
                    taskComplete, mNode.getLocalAddress(), mNode);
            OverlayUtil.logInfo(this.getClass(), "Sent TaskComplete message to Registry.");
        } catch (IOException e) {
            OverlayUtil.logError(this.getClass(), "Error sending task complete message to Registry, "                    + e.getMessage());
        }
    }


    public void handleMessage(Message msg) {
        while(mNode.getState() != MessagingNode.STATE_READY_FOR_ROUTING){
            // wait till it's ready for routing.
        }
        // check if it's a message that needs to be relayed.
        String destination = mNode.getLocalAddress().getHostAddress() + ":" + mNode.getLocalPort();
        String messageDestination = msg.getDestination();
        if (messageDestination.equals(destination)) {
            // this is destined to here
            StatCollector.getInstance().incrementReceiveCount();
            StatCollector.getInstance().incrementReceiveSummation(msg.getValue());
        } else {
            RoutingEntry routingEntry = RoutingRegistry.getInstance().getRoutingEntry(messageDestination);
            String nextHopStr = routingEntry.getNextHop();
            String[] nextHopInfo = nextHopStr.split(":");
            String hostname = nextHopInfo[0];
            int port = Integer.parseInt(nextHopInfo[1]);
            try {
                MessagingUtil.sendMessage(hostname, port, msg, mNode.getLocalAddress(), mNode);
                StatCollector.getInstance().incrementRelayCount();
            } catch (IOException e) {
                OverlayUtil.logError(this.getClass(), "Error relaying message to "
                        + messageDestination + ", " + e.getMessage());
            }
        }
    }

    public void handlePullTrafficSummary(){
        StatCollector statCollector = StatCollector.getInstance();
        TrafficSummary trafficSummary = new TrafficSummary(mNode.getLocalAddress().getHostAddress(),
                mNode.getLocalPort(), statCollector.getSendTracker(), statCollector.getReceiveTracker(),
                statCollector.getRelayTracker(), statCollector.getSendSummation(), statCollector.getReceiveSummation());
        try {
            MessagingUtil.sendMessage(mNode.getRegistryHost(),
                    mNode.getRegistryPort(), trafficSummary, mNode.getLocalAddress(), mNode);
            statCollector.reset();
            OverlayUtil.logInfo(this.getClass(), "Sent Traffic Summary to Registry.");
        } catch (IOException e) {
            OverlayUtil.logError(this.getClass(), "Error sending the traffic summary to registry, " + e.getMessage());
        }
    }

    private void populateVertices(List<OverlayLink> links) {
        // first find the unique nodes
        Set<String> uniqueNodes = new HashSet<String>(links.size());
        for (OverlayLink link : links) {
            uniqueNodes.add(link.getSrc().toString());
            uniqueNodes.add(link.getDestination().toString());
        }

        RoutingRegistry routingRegistry = RoutingRegistry.getInstance();
        String[] nodeIds = uniqueNodes.toArray(new String[uniqueNodes.size()]);
        for (String nodeId : nodeIds) {
            Vertex vertex = new Vertex(nodeId);
            routingRegistry.addDijkstraVertex(vertex);
        }

        String currentNode = mNode.getLocalAddress().getHostAddress() + ":" + mNode.getLocalPort();
        // now we need to process each link and populate link details for vertexes
        for (OverlayLink overlayLink : links) {
            String srcId = overlayLink.getSrc().toString();
            String destId = overlayLink.getDestination().toString();
            Vertex srcVertex = routingRegistry.getVertex(srcId);
            Vertex destVertex = routingRegistry.getVertex(destId);
            Link link = new Link(srcVertex, destVertex, overlayLink.getWeight());
            srcVertex.addLink(link);
            destVertex.addLink(link);
        }

        // wrapping Vertex objects with a weight to be used in the min heap.
        class DijkstraWeightedVertex implements Comparable {
            private Vertex vertex;
            private int weight;

            DijkstraWeightedVertex(Vertex vertex) {
                this.vertex = vertex;
            }

            public int getWeight() {
                return weight;
            }

            public Vertex getVertex() {
                return vertex;
            }

            public void setWeight(int weight) {
                this.weight = weight;
            }

            @Override
            public int compareTo(Object o) {
                return this.getWeight() - ((DijkstraWeightedVertex) o).getWeight();
            }
        }
        // we need a min-heap.
        PriorityQueue<DijkstraWeightedVertex> minHeap = new PriorityQueue<DijkstraWeightedVertex>();
        // now add the initial links to the min-heap
        Vertex[] vertices = routingRegistry.getVertices();
        Map<String, Vertex> unvisited = new HashMap<String, Vertex>(vertices.length);
        for (Vertex vertex : vertices) {
            unvisited.put(vertex.getId(), vertex);
            DijkstraWeightedVertex dijskraLink = new DijkstraWeightedVertex(vertex);
            if (vertex.getId().equals(currentNode)) {
                vertex.setDistance(0);
                dijskraLink.setWeight(0);
            } else {
                // we set 100 in place of infinity because the maximum weight that
                // can be assigned is 10.
                vertex.setDistance(Integer.MAX_VALUE);
                dijskraLink.setWeight(Integer.MAX_VALUE);
            }
            minHeap.add(dijskraLink);
        }

        while (!unvisited.isEmpty()) {
            DijkstraWeightedVertex current = minHeap.poll();
            Vertex currentVertex = current.getVertex();
            List<Link> linksFromVertex = currentVertex.getLinks();
            for (Link link : linksFromVertex) {
                Vertex otherNode = link.getOtherVertex(currentVertex);
                int newDistance = current.getWeight() + link.getWeight();
                if (otherNode.getDistance() > newDistance) {
                    otherNode.setDistance(newDistance);
                    otherNode.setPrevious(currentVertex);
                    otherNode.setDistanceToPrev(link.getWeight());
                    DijkstraWeightedVertex newWeightedVertex = new DijkstraWeightedVertex(otherNode);
                    newWeightedVertex.setWeight(newDistance);
                    minHeap.add(newWeightedVertex);
                }
            }
            unvisited.remove(currentVertex.getId());
        }

        for (Vertex vertex : vertices) {
            if (!vertex.getId().equals(currentNode)) {
                Deque<Vertex> routingSegmentStack = new ArrayDeque<Vertex>();
                Vertex current = vertex;
                while (current != null) {
                    routingSegmentStack.push(current);
                    current = current.getPrevious();
                }
                RoutingEntry routingEntry = new RoutingEntry(currentNode, vertex.getId());
                // pop the src
                routingSegmentStack.pop();
                while (!routingSegmentStack.isEmpty()) {
                    Vertex intermediateNode = routingSegmentStack.pop();
                    routingEntry.addRoutingSegment(intermediateNode.getId(), intermediateNode.getDistanceToPrev());
                }
                routingRegistry.addRoutingEntry(vertex.getId(), routingEntry);
            }
        }

        OverlayUtil.logInfo(this.getClass(), "Completed calculating shortest paths.");
    }
}
