package cs455.overlay.mnode;

import cs455.overlay.dijkstra.Vertex;
import cs455.overlay.node.MessagingNode;
import cs455.overlay.util.MessagingUtil;
import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.impl.DeregistrationRequest;

import java.io.IOException;

/**
 * Author: Thilina
 * Date: 1/31/14
 */
public class CommandHandler {

    private MessagingNode mNode;

    public CommandHandler(MessagingNode mNode) {
        this.mNode = mNode;
    }

    public void handleDeregistration() {
        DeregistrationRequest deRegReq = new DeregistrationRequest();
        deRegReq.setSourceIp(mNode.getLocalAddress().getHostAddress());
        deRegReq.setPort(mNode.getLocalPort());
        try {
            MessagingUtil.sendMessage(mNode.getRegistryHost(), mNode.getRegistryPort(),
                    deRegReq, mNode.getLocalAddress(), mNode);
        } catch (IOException e) {
            OverlayUtil.logError(this.getClass(), "Error sending Deregistration Request." + e.getMessage());
        }
    }

    public void handlePrintShortestPath(){
        RoutingRegistry routingRegistry = RoutingRegistry.getInstance();
        Vertex[] vertices = routingRegistry.getVertices();
        String currentNode = mNode.getLocalAddress().getHostAddress() + ":" + mNode.getLocalPort();
        for(Vertex vertex : vertices){
            if(!vertex.getId().equals(currentNode))
            {
                RoutingEntry routingEntry = routingRegistry.getRoutingEntry(vertex.getId());
                System.out.println(routingEntry);
            }
        }
    }
}
