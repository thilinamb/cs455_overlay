package cs455.overlay.mnode;

import cs455.overlay.dijkstra.Vertex;
import cs455.overlay.type.OverlayLink;
import cs455.overlay.type.OverlayNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Thilina
 * Date: 2/6/14
 */
public class RoutingRegistry {

    private static RoutingRegistry instance = new RoutingRegistry();
    private Map<String, OverlayNode> neighbours = new HashMap<String, OverlayNode>();
    private List<OverlayLink> links = new ArrayList<OverlayLink>();
    // related to Dijkstra algorithm
    private Map<String, Vertex> dijkstraVertices = new HashMap<String, Vertex>();
    private Map<String, RoutingEntry> routingTable = new HashMap<String, RoutingEntry>();

    private RoutingRegistry(){

    }

    public static RoutingRegistry getInstance(){
        return instance;
    }

    public void addNeighbour(OverlayNode node){
        neighbours.put(node.toString(), node);
    }

    public void addLink(OverlayLink link){
        links.add(link);
    }

    public List<OverlayLink> getLinks(){
        return links;
    }

    public void addDijkstraVertex(Vertex v){
        dijkstraVertices.put(v.getId(), v);
    }

    public Vertex getVertex(String id){
        return dijkstraVertices.get(id);
    }

    public Vertex[] getVertices(){
        return dijkstraVertices.values().toArray(new Vertex[dijkstraVertices.size()]);
    }

    public void addRoutingEntry(String dest, RoutingEntry routingEntry){
        routingTable.put(dest, routingEntry);
    }

    public RoutingEntry getRoutingEntry(String dest){
        return routingTable.get(dest);
    }
}
