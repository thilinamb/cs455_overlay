package cs455.overlay.mnode;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Thilina
 * Date: 2/7/14
 */
public class RoutingEntry {
    // represent the destination and distance to it from the previous node
    private class RoutingSegment {

        private String destination;
        private int distance;

        private RoutingSegment(String destination, int distance) {
            this.destination = destination;
            this.distance = distance;
        }

        public String getDestination() {
            return destination;
        }

        public int getDistance() {
            return distance;
        }
    }

    private String src;
    private String dest;
    private List<RoutingSegment> intermediaries = new ArrayList<RoutingSegment>();

    public RoutingEntry(String src, String dest) {
        this.src = src;
        this.dest = dest;
    }

    public void addRoutingSegment(String dest, int distance){
        RoutingSegment routingSegment = new RoutingSegment(dest, distance);
        intermediaries.add(routingSegment);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(src);
        for(RoutingSegment routingSegment : intermediaries){
            stringBuilder.append("--");
            stringBuilder.append(routingSegment.getDistance());
            stringBuilder.append("--");
            stringBuilder.append(routingSegment.getDestination());
        }
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    public String getDest() {
        return dest;
    }

    public String getNextHop(){
        RoutingSegment routingSegment = intermediaries.get(0);
        return routingSegment.destination;
    }
}
