package cs455.overlay.transport;

import cs455.overlay.node.Node;

import java.io.IOException;
import java.net.Socket;

/**
 * Author: Thilina
 * Date: 2/4/14
 */
public class Connection {

    private TCPReceiverThread tcpReceiverThread;
    private TCPSender tcpSender;
    private Node node;
    private Socket socket;

    public Connection(Node node, Socket socket) throws IOException {
        this.node = node;
        this.socket = socket;
        tcpReceiverThread = new TCPReceiverThread(socket, node);
        tcpReceiverThread.start();
        tcpSender = new TCPSender(socket);
    }

    public void sendData(byte[] data) throws IOException {
        tcpSender.sendData(data);
    }
}
