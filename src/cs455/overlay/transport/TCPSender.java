package cs455.overlay.transport;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * User: thilinab
 * Date: 1/29/14
 * Time: 1:49 PM
 */
public class TCPSender {
    private DataOutputStream dataOutputStream;

    public TCPSender(Socket socket) throws IOException {
        this.dataOutputStream = new DataOutputStream(socket.getOutputStream());
    }

    public synchronized void sendData(byte[] data) throws IOException {
        int dataLength = data.length;
        dataOutputStream.writeInt(dataLength);
        dataOutputStream.write(data);
        dataOutputStream.flush();
    }
}
