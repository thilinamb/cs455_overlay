package cs455.overlay.transport;

import cs455.overlay.node.Node;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Author: Thilina
 * Date: 1/31/14
 * ServerConnectionHandler is responsible for listening for incoming
 * connections and handle them. A new ServerThread is created per
 * each incoming connection.
 */
public class ServerConnectionHandler extends Thread {

    private Node node;
    private ServerSocket serverSock;
    private int localPort;

    public ServerConnectionHandler(Node node, int port) throws IOException {
        this.node = node;
        this.localPort = port;
        // create the server socket
        try {
            serverSock = new ServerSocket(port);
        } catch (IOException e) {   // in case of an error, exit.
            throw e;
        }
    }

    public ServerConnectionHandler(Node node) throws IOException {
        this.node = node;
        // create the server socket
        try {
            serverSock = new ServerSocket(0);
            localPort = serverSock.getLocalPort();
        } catch (IOException e) {   // in case of an error, exit.
            throw e;
        }
    }

    @Override
    public void run() {
        // start accepting connections.
        while (true) {
            try {
                Socket socket = serverSock.accept();
                Connection connection = new Connection(node, socket);
            } catch (IOException e) {   // in case if one connection fails, go to the next.
                e.printStackTrace();
                continue;
            }
        }
    }

    public int getLocalPort() {
        return localPort;
    }

}
