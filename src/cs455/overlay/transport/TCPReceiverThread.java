package cs455.overlay.transport;

import cs455.overlay.exception.NoSuchEventException;
import cs455.overlay.node.Node;
import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.Event;
import cs455.overlay.wireformats.EventFactory;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * User: thilinab
 * Date: 1/29/14
 * Time: 1:52 PM
 */
public class TCPReceiverThread extends Thread {
    protected Socket socket;
    protected DataInputStream dataInputStream;
    private Node node;

    public TCPReceiverThread(Socket socket, Node node) throws IOException {
        this.socket = socket;
        dataInputStream = new DataInputStream(socket.getInputStream());
        this.node = node;
    }

    @Override
    public void run() {
        while(socket != null){
            try {
                int dataLength = dataInputStream.readInt();
                if (dataLength > 0) {
                    byte[] data = new byte[dataLength];
                    dataInputStream.readFully(data, 0, dataLength);
                    Event event = EventFactory.getInstance().getEvent(data);
                    node.onEvent(event, socket.getInetAddress().getHostAddress());
                }
            } catch (IOException e) {
                OverlayUtil.logError(this.getClass(), e.getMessage());
                break;
            } catch (NoSuchEventException e) {
                // ignore
            }
        }
    }
}
