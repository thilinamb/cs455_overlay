package cs455.overlay.transport;

import cs455.overlay.node.Node;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Thilina
 * Date: 2/4/14
 * Singleton connection factory maintained at each node.
 * It makes sure that there is only one connection between two nodes in one direction.
 */
public class ConnectionFactory {

    private static ConnectionFactory instance = new ConnectionFactory();

    private Map<String, Connection> connectionMap = new HashMap<String, Connection>();

    private ConnectionFactory(){}

    public static ConnectionFactory getInstance(){
        return instance;
    }

    public synchronized void register(Connection connection, String hostname, int port){
        connectionMap.put(getKey(hostname, port), connection);
    }

    public synchronized Connection getConnection(String hostname, int port, InetAddress srcAddr, Node node)
            throws IOException {
        String key = getKey(hostname, port);
        Connection conn;
        if(connectionMap.containsKey(key)){
            conn = connectionMap.get(key);
        } else {
            Socket socket = new Socket(hostname, port,srcAddr, 0);
            conn = new Connection(node, socket);
            register(conn, hostname, port);
        }
        return conn;
    }

    private String getKey(String hostname, int port){
        return hostname + ":" + port;
    }
}
