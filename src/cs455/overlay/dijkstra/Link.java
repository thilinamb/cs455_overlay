package cs455.overlay.dijkstra;

/**
 * Author: Thilina
 * Date: 2/7/14
 */
public class Link {
    private Vertex src;
    private Vertex dest;
    private int weight;

    public Link(Vertex src, Vertex dest, int weight) {
        this.src = src;
        this.dest = dest;
        this.weight = weight;
    }

    public Vertex getSrc() {
        return src;
    }

    public Vertex getDest() {
        return dest;
    }

    public Vertex getOtherVertex(Vertex current){
        return (src.getId().equals(current.getId())) ? dest : src;
    }

    public int getWeight() {
        return weight;
    }
}
