package cs455.overlay.dijkstra;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Thilina
 * Date: 2/7/14
 */
public class Vertex{
    // this is the src:port which will be the id for the vertex
    private String id;
    // Previous node
    private Vertex previous;
    // set of links from/to this node
    private List<Link> links = new ArrayList<Link>();
    // distance from current node
    private int distance;
    // distance to previous
    private int distanceToPrev;

    public Vertex(String id){
        this.id = id;
    }

    public void setPrevious(Vertex previous) {
        this.previous = previous;
    }

    public void addLink(Link link){
        this.links.add(link);
    }

    public String getId() {
        return id;
    }

    public Vertex getPrevious() {
        return previous;
    }

    public List<Link> getLinks() {
        return links;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistanceToPrev() {
        return distanceToPrev;
    }

    public void setDistanceToPrev(int distanceToPrev) {
        this.distanceToPrev = distanceToPrev;
    }
}
