package cs455.overlay.registry;

import cs455.overlay.node.Registry;
import cs455.overlay.statistics.StatRegistry;
import cs455.overlay.transport.Connection;
import cs455.overlay.type.OverlayLink;
import cs455.overlay.type.OverlayNode;
import cs455.overlay.util.MessagingUtil;
import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.impl.LinkWeights;
import cs455.overlay.wireformats.impl.MessagingNodeList;
import cs455.overlay.wireformats.impl.TaskInitiate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Author: Thilina
 * Date: 2/2/14
 * Command Handler for Registry. Contains methods to take necessary actions upon
 * receiving the supported commands through the console.
 */
public class CommandHandler {

    private Registry registry;

    public CommandHandler(Registry registry) {
        this.registry = registry;
    }

    public void handleListingNodes() {
        OverlayRegistry overlayRegistry = OverlayRegistry.getInstance();
        OverlayNode[] nodes = overlayRegistry.getMessagingNodes();
        if (nodes.length == 0) {
            OverlayUtil.logInfo(this.getClass(), "There are no messaging nodes registered at the moment.");
        } else {
            OverlayUtil.logInfo(this.getClass(), "\tCurrent Messaging Node List");
            OverlayUtil.logInfo(this.getClass(), "----------------------------------");
            for (OverlayNode node : nodes) {
                System.out.println("Hostname: " + node.getHostname() + ", Port: " + node.getPort());
            }
        }
    }

    public void handleListWeights(){
        OverlayRegistry overlayRegistry = OverlayRegistry.getInstance();
        List<OverlayLink> links = overlayRegistry.getOverlayLinks();
        if(links.size() > 0){
            OverlayUtil.logInfo(this.getClass(), "\tOverlay Link Weights");
            OverlayUtil.logInfo(this.getClass(), "----------------------------------");
            for(OverlayLink link : links){
                System.out.println(link.getSrc() + " " +  link.getDestination() + " " + link.getWeight());
            }
        } else {
            OverlayUtil.logInfo(this.getClass(), "There are no links at the moment.");
        }
    }

    public void handleSetupOverlay(int linkCount) {
        OverlayRegistry overlayRegistry = OverlayRegistry.getInstance();
        OverlayNode[] nodes = overlayRegistry.getMessagingNodes();
        // Check of there are 10 nodes to set up the overlay
        if(nodes.length != 10 || nodes.length < (linkCount + 1)){
            OverlayUtil.logError(this.getClass(), "Current node count is " + nodes.length + ", doesn't satisfy the current " +
                    "overlay setup algorithm");
            return;
        }
        // Here we assume that there are only 10 nodes and 4 links from each node.
        // so forming a Keily graph.
        // first set up the ring
        for(int i = 0; i < nodes.length; i++){
            int destIndex = (i + 1) % 10;
            OverlayLink overlayLink = new OverlayLink(nodes[i], nodes[destIndex]);
            overlayRegistry.addOverlayLink(overlayLink);
            nodes[i].addLink(overlayLink);
            nodes[destIndex].addLink(overlayLink);
        }
        // now the second link
        for(int i = 0; i < nodes.length; i = i + 2){
            int destIndex = (i + 2) % 10;
            OverlayLink overlayLink = new OverlayLink(nodes[i], nodes[destIndex]);
            overlayRegistry.addOverlayLink(overlayLink);
            nodes[i].addLink(overlayLink);
            nodes[destIndex].addLink(overlayLink);
        }
        // the third link
        for(int i = 1; i < nodes.length; i = i + 2){
            int destIndex = (i + 2) % 10;
            OverlayLink overlayLink = new OverlayLink(nodes[i], nodes[destIndex]);
            overlayRegistry.addOverlayLink(overlayLink);
            nodes[i].addLink(overlayLink);
            nodes[destIndex].addLink(overlayLink);
        }

        // now send MessageNodeList to each node
        for(OverlayNode node : nodes){
            try {
                sendMessagingNodeListForNode(node, registry);
            } catch (IOException e) {
                OverlayUtil.logError(this.getClass(), "Error sending MESSAGING_NODES_LIST to " +
                        node + ", " + e.getMessage());
            }
        }

        OverlayUtil.logInfo(this.getClass(), "Completed Sending MessageNodeList events.");

    }

    public void handleSendOverlayLinkWeights(){
        OverlayRegistry overlayRegistry = OverlayRegistry.getInstance();
        List<OverlayLink> overlayLinks = overlayRegistry.getOverlayLinks();
        // now assign the weights
        Random random = new Random(System.currentTimeMillis()); // random seed based on time stamp.
        for(OverlayLink link : overlayLinks){
            int weight = random.nextInt(10) + 1; // a random number between 1-10
            link.setWeight(weight);
        }
        // create the message
        LinkWeights linkWeights = new LinkWeights();
        linkWeights.setLinkCount(overlayLinks.size());
        linkWeights.setLinks(overlayLinks);

        //send to each MessagingNode
        OverlayNode[] mNodes = overlayRegistry.getMessagingNodes();
        for(OverlayNode mNode: mNodes){
            Connection connection;
            try {
                MessagingUtil.sendMessage(mNode.getHostname(),
                        mNode.getPort(), linkWeights, registry.getLocalAddress(), registry);
            } catch (IOException e) {
                OverlayUtil.logError(this.getClass(), "Error Sending overlay link weights to "
                        + mNode + ", " + e.getMessage());
            }
        }
        OverlayUtil.logInfo(this.getClass(), "Completed sending overlay link weights.");
    }

    public void handleStart() {
        StatRegistry.getInstance().reset();
        TaskInitiate taskInitiateMsg = new TaskInitiate();
        OverlayRegistry overlayRegistry = OverlayRegistry.getInstance();
        OverlayNode[] mNodes = overlayRegistry.getMessagingNodes();
        // send TASK_INITIATE message to each messaging node.
        for(OverlayNode mNode: mNodes){
            Connection connection;
            try {
                MessagingUtil.sendMessage(mNode.getHostname(),
                        mNode.getPort(), taskInitiateMsg, registry.getLocalAddress(), registry);
                overlayRegistry.addNackNode(mNode.getHostname(), mNode.getPort());
            } catch (IOException e) {
                OverlayUtil.logError(this.getClass(), "Error sending task initiate message to node "
                        + mNode.toString() + ", " + e.getMessage());
            }
        }
        OverlayUtil.logInfo(this.getClass(), "Completed sending TASK INITIATE Messages.");
    }

    private void sendMessagingNodeListForNode(OverlayNode node, Registry registry) throws IOException {
        List<OverlayLink> links = node.getLinks();
        List<OverlayNode> nodes = new ArrayList<OverlayNode>(links.size());
        for(OverlayLink link : links){
            OverlayNode other = (node.equals(link.getSrc())) ? link.getDestination() : link.getSrc();
            nodes.add(other);
        }
        MessagingNodeList messagingNodeList = new MessagingNodeList();
        messagingNodeList.setNodeCount(nodes.size());
        messagingNodeList.setNodes(nodes);

        MessagingUtil.sendMessage(node.getHostname(),
                node.getPort(), messagingNodeList, registry.getLocalAddress(), registry);
    }
}

