package cs455.overlay.registry;

import cs455.overlay.node.Registry;
import cs455.overlay.statistics.StatRegistry;
import cs455.overlay.type.OverlayNode;
import cs455.overlay.util.MessagingUtil;
import cs455.overlay.util.OverlayUtil;
import cs455.overlay.wireformats.impl.*;

import java.io.IOException;

/**
 * Author: Thilina
 * Date: 1/31/14
 * Event handler for registry. Acts on various events received by
 * a registry.
 */
public class EventHandler {

    private Registry registry;
    private final StatRegistry statRegistry = StatRegistry.getInstance();
    private final OverlayRegistry overlayRegistry = OverlayRegistry.getInstance();

    public EventHandler(Registry registry) {
        this.registry = registry;
    }

    public void handleRegistrationReq(RegistrationRequest event, String sourceIp) {
        RegistrationResponse resp = new RegistrationResponse();
        // validate the host first
        String host = event.getSourceIpAddress();
        OverlayNode node = null;
        int port = 0;
        boolean success = false;
        if (!host.equals(sourceIp)) {
            resp.setSuccess(false);
            resp.setMessage("Invalid Source Address.");
        } else {
            port = event.getPort();
            synchronized (overlayRegistry) {
                if (!overlayRegistry.containsNode(host, port)) {   // Check for duplicate Nodes
                    node = new OverlayNode(host, port);
                    overlayRegistry.addNode(node);
                    resp.setSuccess(true);
                    resp.setMessage("Registration is successful. The number of nodes in the overlay is " +
                            overlayRegistry.getNodeCount());
                    success = true;
                } else { // Duplicate Node
                    resp.setSuccess(false);
                    resp.setMessage("Duplicate Node!");
                }
            }
        }
        // send the response back to the messaging node.
        try {
            MessagingUtil.sendMessage(host, port, resp, registry.getLocalAddress(), registry);
        } catch (IOException e) {
            // in case of error, remove it from the overlay registry.
            if (success) {
                overlayRegistry.remove(host, port);
            }
            OverlayUtil.logError(this.getClass(), "Error sending Registration Response to "
                    + node + ", " + e.getMessage());
        }
    }

    public void handleDeregistration(DeregistrationRequest event, String sourceIp) {
        DeregistrationResponse response = new DeregistrationResponse();
        OverlayNode node = null;
        boolean success = false;
        // check the authenticity of the source
        if (!event.getSourceIp().equals(sourceIp)) {
            response.setSuccess(false);
            response.setMessage("Source Address mismatch!");
        } else {
            if (!overlayRegistry.containsNode(event.getSourceIp(), event.getPort())) {   // Check whether the node exists
                response.setSuccess(false);
                response.setMessage("Node hasn't been registered before.");
            } else {
                node = overlayRegistry.remove(event.getSourceIp(), event.getPort());
                response.setSuccess(true);
                response.setMessage("Deregistration is successful!");
                success = true;
            }
        }
        // inform the client
        try {
            MessagingUtil.sendMessage(event.getSourceIp(), event.getPort(), response,
                    registry.getLocalAddress(), registry);
        } catch (IOException e) {
            // revert the operation in case of failing to notify
            if(success){
                overlayRegistry.addNode(node);
            }
            OverlayUtil.logError(this.getClass(), "Error sending Deregistration Response to "
                    + OverlayUtil.getNodeName(event.getSourceIp(), event.getPort()) + ", " + e.getMessage());
        }
    }

    public void handleTaskComplete(TaskComplete taskComplete) {
        String hostname = taskComplete.getHostAddress();
        int port = taskComplete.getPort();
        OverlayUtil.logInfo(this.getClass(), "Received Task Complete message from " +
                OverlayUtil.getNodeName(hostname, port));
        int nackNodeCount = overlayRegistry.removeNackNode(hostname, port);
        if (nackNodeCount == 0) {
            try {   // sleep for 5 seconds
                Thread.sleep(5 * 1000);
            } catch (InterruptedException ignored) {

            }
            // initiate pull traffic summary
            PullTrafficSummary pullTrafficSummary = new PullTrafficSummary();
            for (OverlayNode mNode : overlayRegistry.getMessagingNodes()) {
                try {
                    MessagingUtil.sendMessage(mNode.getHostname(), mNode.getPort(),
                            pullTrafficSummary, registry.getLocalAddress(), registry);
                } catch (IOException e) {
                    OverlayUtil.logError(this.getClass(), "Error sending PullTrafficRequest to " +
                            mNode + ", " + e.getMessage());
                }
            }
            OverlayUtil.logInfo(this.getClass(), "Completed sending Pull Traffic Summary Requests.");
        }
    }

    public void handleTrafficSummary(TrafficSummary ts) {
        OverlayUtil.logInfo(this.getClass(), "Received Traffic Summary from " +
                OverlayUtil.getNodeName(ts.getHostAddress(), ts.getPort()));
        synchronized (statRegistry) {
            int summaryCount = statRegistry.processTrafficSummary(ts);
            if (summaryCount == overlayRegistry.getNodeCount()) {
                statRegistry.printTrafficSummary();
            }
        }
    }

}