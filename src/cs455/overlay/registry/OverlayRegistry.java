package cs455.overlay.registry;

import cs455.overlay.type.OverlayLink;
import cs455.overlay.type.OverlayNode;
import cs455.overlay.util.OverlayUtil;

import java.util.*;

/**
 * Author: Thilina
 * Date: 1/31/14
 * A registry to keep track of the overlay such as nodes, links and link weights.
 * This is a singleton.
 */
public class OverlayRegistry {

    private static final OverlayRegistry instance = new OverlayRegistry();

    // This is the list of nodes registered.
    private Map<String, Integer> nodeCatalog = new Hashtable<String, Integer>();
    // This is the set of overlay links
    private List<OverlayLink> overlayLinks = new ArrayList<OverlayLink>();
    // Each node is given a node id, map it with a node.
    private Map<Integer, OverlayNode> nodes = new HashMap<Integer, OverlayNode>();
    // Keep track of the nodes that haven't acked on task completion.
    private ArrayList<String> nackNodes = new ArrayList<String>();

    private OverlayRegistry(){

    }

    public static OverlayRegistry getInstance(){
        return instance;
    }

    public synchronized void addNode(OverlayNode node){
        nodes.put(node.getNodeId(), node);
        nodeCatalog.put(node.toString(), node.getNodeId());
    }

    public synchronized int getNodeCount(){
        return nodes.size();
    }

    public synchronized boolean containsNode(String hostname, int port){
        String nodeName = OverlayUtil.getNodeName(hostname, port);
        return nodeCatalog.containsKey(nodeName);
    }

    public synchronized OverlayNode remove(String hostname, int port){
        String nodeName = OverlayUtil.getNodeName(hostname, port);
        int nodeId = nodeCatalog.get(nodeName);
        nodeCatalog.remove(nodeName);
        return nodes.remove(nodeId);
    }

    public OverlayNode[] getMessagingNodes(){
        OverlayNode[] nodeArray = new OverlayNode[nodes.size()];
        return nodes.values().toArray(nodeArray);
    }

    // this method is invoked by a single thread. So not required to synchronize
    public void addOverlayLink(OverlayLink link){
        overlayLinks.add(link);
    }

    public List<OverlayLink> getOverlayLinks(){
        return overlayLinks;
    }

    public synchronized void addNackNode(String hostname, int port){
        nackNodes.add(OverlayUtil.getNodeName(hostname, port));
    }

    public synchronized int removeNackNode(String hostname, int port){
        nackNodes.remove(OverlayUtil.getNodeName(hostname, port));
        return nackNodes.size();
    }
}
